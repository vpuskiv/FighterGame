# FighterGame
I'm trying to create a fighting game, using Shawnthebro's tutorial. It suppose to be a University project, but I hope I'll be able to drag it further than that.
I also use Mixamo Characters and Animations.

![](https://gitlab.com/vpuskiv/FighterGame/-/raw/stable/FighterGame.png)

# Project Progress:
## Planned Features Progress:
![](https://progress-bar.dev/61?width=500)<br/>

### UI/UX:
![](https://progress-bar.dev/80?width=300)<br/>
```diff
+ HUD
+ Character Select Design
+ Functional Character Select
+ Stage (Level) Select Screen
+ Super Meter & Super Gauge
+ Input/Command Buffers (Input History Display)
+ Multiplayer Character Select Menu
+ Multiplayer Stage Select Menu
+ 3D Character Selection Screen
+ Stage Select Screen Preview
- Character Skins & Outfits
+ Menu Control for Player 2 (Gamepad)
+ Pause Menu + Stopping Time
- Random Character/Stage Select
- Advanced Health Bar (Smooth Decay)
```

### Game Flow:
![](https://progress-bar.dev/67?width=300)<br/>
```diff
+ Basic state.
+ Dynamic Hitbox Creation
+ Dynamic Hitbox Collision
+ Spawn Points
+ Dynamic Player Spawns
+ Hurtboxes & Hit Detection
+ Make Player Faces Enemy
+ Directional Input Attacks
+ Combo System
+ Fighting Game Camera (Follow players, Zoom in and out)
+ Local Multiplayer (Separate Instances)
+ Local Multiplayer (Control Player 2)
+ Controls Remapping & Menu
+ Fighting Game Camera (Boundaries)
+ Showing Damage On Characters
+ Meter Burn & Super Meter Use
+ Input/Command Buffers (Sequence Container)
+ Gamepad + Controller Support
+ Input/Command Buffers (Player 2 Input History & Diagonal Inputs)
+ Menu Flow & Main Menu
+ Round Timer
+ Round Counter
+ Winning Match
+ Advanced Hitboxes
+ Modifying Hurtbox + Collider
- Wall Bounces + Ground Bounces
- Advanced Input System
- Advanced Input System (Player 2)
- Advanced Camera Mechanics
- Advanced Input System (Mirror Input Buffer for Player 2)
- Advanced Input System (Double Input)
- Quick & Easy Add New Characters
- Dynamic Move List
- Dynamic Move List (Scroll + Controller Support)
- Pushing Characters
- Low, High, and Overhead Attacks
- Implementing Hitstop
```

### Animations:
![](https://progress-bar.dev/82?width=300)<br/>
```diff
+ Animations & State Machines
+ Movement Animationss
+ Directional Jumps
+ Hitstun & Hit Reactions
+ Blocking (By Walking Back)
+ Pushback/Knockback
+ Launchers
+ Super Moves/Ultimates
+ Input/Command Buffers (Perform Animation)
+ Input/Command Buffers (Perform Multiple Attacks)
+ Super Moves/Ultimates (Land a Hit and Trigger a Cutscene)
+ Super Moves/Ultimates (Player 2 Super)
+ Projectiles
+ Throws & Grapples
+ Character Entrances & Intros
+ Throws + Grapples Logic
+ Using Root Motion
+ Knockdowns + Wake-Ups
+ Crouch Attacks + Logic
- Projectiles + Special Moves (Spawn Projectile under the opponent)
- Wall Bounces + Ground Bounces
- Projectiles + Special Moves (Chasing Projectile)
- Forward and Backward Dashing
```

### Sound Effects:
![](https://progress-bar.dev/16?width=300)<br/>
```diff
+ Getting Hurt Voice
- Hitsound
- Menu Sound
- Match Begin Voiceover
- Using Supper Voice
- Yelling When Using Move  
```

### List of Current Bugs:
```diff
- Rematch button (in pause menu) Doesn't Work.
- X Axis is not locked, so the characters can be pushed to the back/front and won't be able to attack.
- Some 'Combo Moves' can be buffered at the start of the match.
- Super Move can be buffered without EX Meter.
- Animation of Getting Hit by a Grab is broken, it can thrown you to the side.
- Sometimes, characters might spawn at the wrong X axis.
- Getting Hit in Crouch State triggers Standing Hit Animation, instead of Crouch one.
- After Match Camera isn't adjusted correctly.
- After Match Hurtboxes aren't cleaned.
- After Match Character Select openes Main Menu (error: Character Select Widget is already oppened).
+ After Match Menu doesn't appear.
```