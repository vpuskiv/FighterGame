// Copyright Epic Games, Inc. All Rights Reserved.

#include "FighterGameGameMode.h"
#include "FighterGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFighterGameGameMode::AFighterGameGameMode()
{
	//// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	//if (PlayerPawnBPClass.Class != NULL)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
	roundTimer = 60;
	numRounds = 3;
	isTimerActive = false;
}

void AFighterGameGameMode::CheatFullHealAll()
{
	if (player1)
	{
		player1->SetHealth(1.0f);
	}

	if (player2)
	{
		player2->SetHealth(1.0f);
	}
}

void AFighterGameGameMode::CheatFullHealPlayer(int _playerNumber)
{
	if (_playerNumber == 1)
	{
		if (player1)
		{
			player1->SetHealth(1.0f);
		}

	}
	else if(_playerNumber == 2)
	{
		if (player2)
		{
			player2->SetHealth(1.0f);
		}
	}
}

void AFighterGameGameMode::CheatFullSuperAll()
{
	if (player1)
	{
		player1->SetSuperMeter(1.0f);
	}

	if (player2)
	{
		player2->SetSuperMeter(1.0f);
	}
}

void AFighterGameGameMode::CheatFullSuperPlayer(int _playerNumber)
{
	if (_playerNumber == 1)
	{
		if (player1)
		{
			player1->SetSuperMeter(1.0f);
		}

	}
	else if (_playerNumber == 2)
	{
		if (player2)
		{
			player2->SetSuperMeter(1.0f);
		}
	}
}

void AFighterGameGameMode::CheatKillPlayer(int _playerNumber)
{
	if (_playerNumber == 1)
	{
		if (player1)
		{
			player1->CheatTakeDamage(1.0f);
		}
	}
	else if (_playerNumber == 2)
	{
		if (player2)
		{
			player2->CheatTakeDamage(1.0f);
		}
	}
}