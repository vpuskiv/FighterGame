// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FighterGameCharacter.h"
#include "MannequinCharacter.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTERGAME_API AMannequinCharacter : public AFighterGameCharacter
{
	GENERATED_BODY()
public:
	AMannequinCharacter();
};
