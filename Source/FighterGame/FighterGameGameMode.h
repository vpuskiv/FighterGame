// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FighterGameCharacter.h"
#include "FighterGameGameMode.generated.h"

UCLASS(minimalapi)
class AFighterGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFighterGameGameMode();

	// Cheat: Give all players full HP
	UFUNCTION(Exec, Category = ExecFunctions)
	void CheatFullHealAll();

	// Cheat: Give the specific player full HP
	UFUNCTION(Exec, Category = ExecFunctions)
	void CheatFullHealPlayer(int _playerNumber);

	// Cheat: Give all players full Super Meter
	UFUNCTION(Exec, Category = ExecFunctions)
	void CheatFullSuperAll();

	// Cheat: Give the specific player full Super Meter
	UFUNCTION(Exec, Category = ExecFunctions)
	void CheatFullSuperPlayer(int _playerNumber);

	// Cheat: deals 100% damage to the player
	UFUNCTION(Exec, Category = ExecFunctions)
	void CheatKillPlayer(int _playerNumber);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
	AFighterGameCharacter* player1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
	AFighterGameCharacter* player2;

	// The timer for each round
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMode Settings")
	float roundTimer;

	// The number of round each fight lasts
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMode Settings")
	int numRounds;

	// Should the round timer continuously go down at the current time? (Pause timer when super plays, fight starting, ect.)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMode Settings")
	bool isTimerActive;
};



