// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BaseGameInstance.generated.h"

UENUM(BlueprintType)
enum class ECharacterClass : uint8
{
	VE_Default	UMETA(DisplayName = "Mannequin"),
	VE_Lola		UMETA(DisplayName = "Lola")
};

UCLASS()
class FIGHTERGAME_API UBaseGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
	ECharacterClass P1CharacterClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
	ECharacterClass P2CharacterClass;

	// The timer for each round (this value can be overriten by player in Options Menu)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameInstance Settings")
	float roundTimer;

	// The number of round each fight lasts (this value can be overriten by player in Options Menu)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameInstance Settings")
	float numRounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
	bool isDeviceForMultiplePlayers;
};
